<?php
/**
 * Created by Evgeniy Kazak
 * User: ekazak
 * Date: 12.01.18
 * Time: 11:32
 */

namespace Exonum;


class Api
{
    private $_services = [
        'system' => 'Exonum\services\System'
    ];

    private $_active_services = [];

    public function __construct()
    {

    }

    public function service($service)
    {
        if (is_string($service) && isset($this->_active_services[$service])) {
            return $this->_active_services[$service];
        } elseif (is_string($service) && isset($this->_services[$service])) {
            $service = new $this->_services[$service]();
            var_dump($service);
        }
    }
}