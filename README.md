Exonum API for PHP
=============

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require ekazak/exonumapi:dev-master
```

or add

```
"ekazak/exonumapi": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

All info about methods you can find here: